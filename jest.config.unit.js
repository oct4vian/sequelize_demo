module.exports = {
  displayName: { name: 'UNIT_TEST', color: 'yellow' },
  testMatch: ['**/src/__tests__/unit-tests/**/*.test.js']
};
