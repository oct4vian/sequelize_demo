# SEQUELIZE DEMO APPLICATION

### Steps to configure the project locally

- !!! Make sure you have a local postgreSQL server installed !!!
- Go to `config` folder and create a new file called `dev.env`. Copy the contents from `.env.example` inside. Edit the values of the keys with the correct connection information for your postgreSQL server.
- Run `npm install` in the root directory
- Run `npm run db:init`. This will attempt to create the database specified in `dev.env` and run the migrations.
- Run the application using `npm run start`.
  - _Optional_: have `nodemon` installed (globally) and start the application using `nodemon` command.

### Writing tests

The application is configured to run 2 types of tests:

- **Unit tests** (with the command `npm run test`. `npm run test:w` can also be used; this command will start the test runner in watch mode, so that every change you make in the tests will trigger a rerun of the tests)

**IMPORTANT**: Run the `npm run db:init:test` command before running the tests for the first time. It will attempt to create a test database and run migrations based on the configurations specified in `dev.env` file. The test database will have the same name as the development database, but with the `_test` suffix.

- **Integration tests** (with the command `npm run test:i`)

##### Unit tests

They should be as small as possible and only test a part of the application (i.e. a function or a method). All dependencies should be mocked using **jest**.

##### Integration tests

They test the whole system. They are run against a new, fresh database each time. Nothing is mocked. When the integration tests are run, a (new) test database is created, migrated and seeded in order to have consistent data. After the tests are run, the database is dropped. All these actions are abstracted in the `test:i` script.
Integration tests should call the api directly. In order to do this we use the `supertest` library.
