module.exports = {
  displayName: { name: 'INTEGRATION_TEST', color: 'white' },
  testMatch: ['**/src/__tests__/integration-tests/**/*.test.js']
};
