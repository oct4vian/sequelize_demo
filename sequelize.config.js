require('dotenv').config({ path: './config/dev.env' });

const { DB_NAME, DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DIALECT } = process.env;

module.exports = {
  development: {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    dialect: DB_DIALECT
  },
  test: {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: `${DB_NAME}_test`,
    host: DB_HOST,
    dialect: DB_DIALECT
  },
  'i-test': {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: `${DB_NAME}_integration_test`,
    host: DB_HOST,
    dialect: DB_DIALECT
  }
};
