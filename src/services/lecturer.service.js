const dao = require('../persistence/lecturer.dao');

module.exports = {
  async getAll() {
    return dao.getAll();
  },

  async getOne(id) {
    return dao.getOne(id);
  },

  async create(lecturer_name) {
    return dao.create(lecturer_name);
  },

  async update(id, lecturer_name) {
    return dao.update(id, lecturer_name);
  },

  async delete(id) {
    return dao.delete(id);
  }
};
