const dao = require('../persistence/course.dao');

module.exports = {
  async getAll() {
    return dao.getAll();
  },

  async getOne(id) {
    return dao.getOne(id);
  },

  async create(course_name) {
    return dao.create(course_name);
  },

  async update(id, course_name) {
    return dao.update(id, course_name);
  },

  async delete(id) {
    return dao.delete(id);
  }
};
