const classroomService = require('./classroom.service');
const courseService = require('./course.service');
const lecturerService = require('./lecturer.service');
const studentService = require('./student.service');

module.exports = {
  classroomService,
  courseService,
  lecturerService,
  studentService
};
