const dao = require('../persistence/classroom.dao');

module.exports = {
  async getAll() {
    return dao.getAll();
  },

  async getOne(id) {
    return dao.getOne(id);
  },

  async create(class_name) {
    return dao.create(class_name);
  },

  async update(id, class_name) {
    return dao.update(id, class_name);
  },

  async delete(id) {
    return dao.delete(id);
  },

  async createWithStudents(class_name, students) {
    return dao.createWithStudents(class_name, students);
  }
};
