const dao = require('../persistence/student.dao');
const courseService = require('./course.service');

module.exports = {
  async getAll(page = 0) {
    return dao.getAll(page);
  },

  async getOne(id) {
    return dao.getOne(id);
  },

  async create(student_name) {
    return dao.create(student_name);
  },

  async createWithStudents(class_name, students) {
    return dao.createWithStudents(class_name, students);
  },

  async update(id, student_name) {
    return dao.update(id, student_name);
  },

  async delete(id) {
    return dao.delete(id);
  },

  async addCourse(student_id, course_id) {
    const student = await dao.getOne(student_id);

    if (!student) {
      throw new Error({ message: 'No student with the specified id found' });
    }

    const course = await courseService.getOne(course_id);

    if (!course) {
      throw new Error({ message: 'No course with the specified id found' });
    }

    return student.addCourse(course);
  }
};
