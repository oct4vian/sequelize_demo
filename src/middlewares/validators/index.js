const validate = require('./validate');
const studentRules = require('./student.validator');

module.exports = {
  validate,
  studentRules
};
