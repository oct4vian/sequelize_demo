const { check } = require('express-validator');

const studentRules = [
  check('student_name')
    .isLength({ min: 5 })
    .withMessage("Student's name must be at least 5 characters long")
];

module.exports = studentRules;
