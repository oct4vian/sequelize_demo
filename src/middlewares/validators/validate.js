const { validationResult } = require('express-validator');

const validate = (req, res, next) => {
  const result = validationResult(req);

  if (result.isEmpty()) {
    return next();
  }

  const errors = result.errors.map(e => ({ [e.param]: e.msg }));

  return res.status(422).json({ errors });
};

module.exports = validate;
