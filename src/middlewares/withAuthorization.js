const withAuthorization = (req, res, next) => {
  // Check if user is authenticated using your own strategy
  // Here we just check if the user has "authorization" header

  // If the user is not authenticated, return a 401 error code
  if (!req.headers.authorization) {
    return res.status(401).end();
  }

  // Else, continue with the request
  next();
};

module.exports = withAuthorization;
