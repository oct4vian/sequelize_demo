const { ErrorHandler } = require('../helpers/error');
const { lecturerService } = require('../services');

module.exports = {
  async getAll(_, res) {
    try {
      const { rows: lecturers, count } = await lecturerService.getAll();

      return res.status(200).send({ lecturers, count });
    } catch (error) {
      next(error);
    }
  },

  async getOne(req, res, next) {
    const { id } = req.params;

    try {
      const result = await lecturerService.getOne(id);

      if (!result) {
        const message = `No lecturer with the specified id (${id}) found`;
        throw new ErrorHandler(404, message);
      }

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async create(req, res, next) {
    const { lecturer_name } = req.body;

    try {
      const result = await lecturerService.create(lecturer_name);

      return res.status(201).send(result);
    } catch (error) {
      next(error);
    }
  },

  async update(req, res, next) {
    const { id } = req.params;
    const { lecturer_name } = req.body;

    try {
      const result = await lecturerService.update(id, lecturer_name);

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async delete(req, res, next) {
    const { id } = req.params;

    try {
      const result = await lecturerService.delete(id);

      return res.status(204).send(result);
    } catch (error) {
      next(error);
    }
  }
};
