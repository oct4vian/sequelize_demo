const classroomController = require('./classroom.controller');
const studentController = require('./student.controller');
const lecturerController = require('./lecturer.controller');
const courseController = require('./course.controller');

module.exports = {
  classroomController,
  studentController,
  lecturerController,
  courseController
};
