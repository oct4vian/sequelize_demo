const { ErrorHandler } = require('../helpers/error');
const { classroomService } = require('../services');

module.exports = {
  async getAll(req, res, next) {
    try {
      const { rows: classrooms, count } = await classroomService.getAll();

      return res.status(200).send({ classrooms, count });
    } catch (error) {
      next(error);
    }
  },

  async getOne(req, res, next) {
    const { id } = req.params;

    try {
      const result = await classroomService.getOne(id);

      if (!result) {
        const message = `No classroom with the specified id (${id}) found`;
        throw new ErrorHandler(404, message);
      }

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async create(req, res, next) {
    const { class_name } = req.body;

    try {
      const result = await classroomService.create(class_name);

      return res.status(201).send(result);
    } catch (error) {
      next(error);
    }
  },

  async update(req, res, next) {
    const { id } = req.params;
    const { class_name } = req.body;

    try {
      const result = await classroomService.update(id, class_name);

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async delete(req, res, next) {
    const { id } = req.params;

    try {
      const result = await classroomService.delete(id);

      return res.status(204).send(result);
    } catch (error) {
      next(error);
    }
  },

  async createWithStudents(req, res, next) {
    const { class_name, students } = req.body;

    try {
      const result = await classroomService.createWithStudents(
        class_name,
        students
      );

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  }
};
