const { ErrorHandler } = require('../helpers/error');
const { courseService } = require('../services');

module.exports = {
  async getAll(_, res) {
    try {
      const { rows: courses, count } = await courseService.getAll();

      return res.status(200).send({ courses, count });
    } catch (error) {
      next(error);
    }
  },

  async getOne(req, res, next) {
    const { id } = req.params;

    try {
      const result = await courseService.getOne(id);

      if (!result) {
        const message = `No course with the specified id (${id}) found`;
        throw new ErrorHandler(404, message);
      }

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async create(req, res, next) {
    const { course_name } = req.body;

    try {
      const result = await courseService.create(course_name);

      return res.status(201).send(result);
    } catch (error) {
      next(error);
    }
  },

  async update(req, res, next) {
    const { id } = req.params;
    const { course_name } = req.body;

    try {
      const result = await courseService.update(id, course_name);

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async delete(req, res, next) {
    const { id } = req.params;

    try {
      const result = await courseService.delete(id);

      return res.status(204).send(result);
    } catch (error) {
      next(error);
    }
  }
};
