const { ErrorHandler } = require('../helpers/error');
const { studentService } = require('../services');

module.exports = {
  async getAll(req, res, next) {
    const { page } = req.query;

    try {
      const { rows: students, count } = await studentService.getAll(page);

      return res.status(200).send({ students, count });
    } catch (error) {
      next(error);
    }
  },

  async getOne(req, res, next) {
    const { id } = req.params;

    try {
      const result = await studentService.getOne(id);

      if (!result) {
        const message = `No student with the specified id (${id}) found`;
        throw new ErrorHandler(404, message);
      }

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async create(req, res, next) {
    const { student_name } = req.body;

    try {
      const result = await studentService.create(student_name);

      return res.status(201).send(result);
    } catch (error) {
      next(error);
    }
  },

  async update(req, res, next) {
    const { id } = req.params;
    const { student_name } = req.body;

    try {
      const result = await studentService.update(id, student_name);

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  },

  async delete(req, res, next) {
    const { id } = req.params;

    try {
      const result = await studentService.delete(id);

      return res.status(204).send(result);
    } catch (error) {
      next(error);
    }
  },

  async addCourse(req, res, next) {
    const { student_id, course_id } = req.body;

    try {
      const result = await studentService.addCourse(student_id, course_id);

      return res.status(200).send(result);
    } catch (error) {
      next(error);
    }
  }
};
