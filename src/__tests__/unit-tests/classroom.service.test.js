// UNIT TEST
// This unit test uses sequelize model to insert data into the database
// This is more like a INTEGRATION TEST, but it can be run in watch mode

const classroomService = require('../../services/classroom.service');
const db = require('../../models/index');

describe('Classroom service tests', () => {
  beforeEach(async () => {
    // Clear the classroom table before each test is run
    await db.Classroom.destroy({ where: {}, truncate: true });
  });

  afterAll(() => db.sequelize.close());

  test('should return 0 classrooms', async () => {
    const { rows: classrooms, count } = await classroomService.getAll();

    expect(classrooms.length).toEqual(0);
    expect(count).toEqual(0);
  });

  test('should add a classroom', async () => {
    const classroomToAdd = { class_name: 'class1' };

    const { count: initialCount } = await classroomService.getAll();
    const result = await classroomService.create(classroomToAdd.class_name);
    const { count: countAfterInsert } = await classroomService.getAll();

    expect(result).toBeDefined();
    expect(initialCount + 1).toEqual(countAfterInsert);
  });

  test('should update the name of one classroom', async () => {
    const initialName = 'class1';
    const newName = 'new_class_name';

    const classroom = await classroomService.create(initialName);
    const result = await classroomService.update(classroom.id, newName);

    expect(result).toBeDefined();
    expect(result.class_name).toEqual(newName);
    expect(result.class_name).not.toEqual(initialName);
  });
});
