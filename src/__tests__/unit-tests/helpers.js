module.exports = {
  students: [
    { id: 1, student_name: 'stud1' },
    { id: 2, student_name: 'stud2' },
    { id: 3, student_name: 'stud3' },
    { id: 4, student_name: 'stud4' },
    { id: 5, student_name: 'stud5' },
    { id: 6, student_name: 'stud6' },
    { id: 7, student_name: 'stud7' },
    { id: 8, student_name: 'stud8' },
    { id: 9, student_name: 'stud9' },
    { id: 10, student_name: 'stu10' },
    { id: 11, student_name: 'stud11' }
  ]
};
