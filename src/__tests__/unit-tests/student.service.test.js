jest.mock('../../models');

const models = require('../../models');
const { Student } = models;

const studentService = require('../../services/student.service');
const { students } = require('./helpers');

describe('Student service tests', () => {
  test('should return the first 5 students and a count of 11', async () => {
    Student.findAndCountAll.mockImplementationOnce(
      async ({ offset, limit }) => ({
        rows: students.slice(offset, limit),
        count: students.length
      })
    );

    const { rows, count } = await studentService.getAll(0);

    expect(count).toEqual(11);
    expect(rows.length).toEqual(5);
    expect(rows[0].id).toEqual(1);
  });

  test("should return the student with name 'stud3'", async () => {
    const objectUnderTest = students[2];
    Student.findByPk.mockResolvedValueOnce(objectUnderTest);

    const result = await studentService.getOne(objectUnderTest.id);

    expect(result.student_name).toEqual('stud3');
  });

  test('should return null when no student with specified id available', async () => {
    Student.findByPk.mockResolvedValueOnce(null);

    const result = await studentService.getOne(1234);

    expect(result).toBeNull();
  });
});
