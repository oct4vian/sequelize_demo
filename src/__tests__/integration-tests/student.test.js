const request = require('supertest');
const app = require('../../app');

const db = require('../../models/index');

// the afterAll() method must close the DB connection in order to allow Jest to exit successfully.

describe('Students integration tests using seeds', () => {
  const agent = request(app);
  const apiUrl = '/api/students';

  afterAll(() => db.sequelize.close());

  test('GET /api/students should return status code 200, 5 students and total count (100)', async () => {
    // Act
    const result = await agent.get(apiUrl);

    // Assert
    expect(result.status).toEqual(200);
    expect(result.body.students.length).toEqual(5);
    expect(result.body.count).toEqual(100);
  });

  test('POST /api/students should insert the student in the database', async () => {
    // Arrange
    const studentToInsert = { student_name: 'inserted student' };

    // Act
    const studentsCountBeforeInsert = (await agent.get(apiUrl)).body.count;
    const insertResult = await agent.post(apiUrl).send(studentToInsert);
    const studentsCountAfterInsert = (await agent.get(apiUrl)).body.count;

    // Assert
    expect(insertResult.status).toEqual(201);
    expect(insertResult.body.id).toBeDefined();
    expect(studentsCountAfterInsert).toEqual(studentsCountBeforeInsert + 1);
  });

  test('DELETE /api/students/10 should delete the student from the database', async () => {
    // Arrange
    const studentToDelete = 10;

    // Act
    const studentsCountBeforeDelete = (await agent.get(apiUrl)).body.count;
    const deleteResult = await agent.delete(`${apiUrl}/${studentToDelete}`);
    const studentsCountAfterDelete = (await agent.get(apiUrl)).body.count;

    // Assert
    expect(deleteResult.status).toEqual(204);
    expect(studentsCountAfterDelete).toEqual(studentsCountBeforeDelete - 1);
  });
});
