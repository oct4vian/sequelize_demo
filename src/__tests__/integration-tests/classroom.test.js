const request = require('supertest');
const app = require('../../app');

const db = require('../../models/index');

// The afterAll() method must close the DB connection in order to allow Jest to exit successfully.

describe('Classrooms integration tests using sequelize models instead of seeds', () => {
  const agent = request(app);
  const apiUrl = '/api/classrooms';

  beforeEach(async () => {
    // Clear the classroom table before each test is run
    await db.Classroom.destroy({ where: {}, truncate: true });
  });

  afterAll(() => db.sequelize.close());

  test('GET /api/classrooms should return status code 200, 0 classrooms and total count (0)', async () => {
    // Act
    const result = await agent.get(apiUrl);

    // Assert
    expect(result.status).toEqual(200);
    expect(result.body.classrooms.length).toEqual(0);
    expect(result.body.count).toEqual(0);
  });

  test('GET /api/classrooms/:id should return status code 404 because no classrooms in the db', async () => {
    // Act
    const result = await agent.get(`${apiUrl}/1`);

    // Assert
    expect(result.status).toEqual(404);
  });

  test('POST /api/classrooms should insert the classroom in the database', async () => {
    // Arrange
    const classroomToInsert = { class_name: 'inserted classroom' };

    // Act
    const classroomsCountBeforeInsert = (await agent.get(apiUrl)).body.count;
    const insertResult = await agent.post(apiUrl).send(classroomToInsert);
    const classroomsCountAfterInsert = (await agent.get(apiUrl)).body.count;

    // Assert
    expect(insertResult.status).toEqual(201);
    expect(insertResult.body.id).toBeDefined();
    expect(classroomsCountAfterInsert).toEqual(classroomsCountBeforeInsert + 1);
  });
});
