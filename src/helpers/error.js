const chalk = require('chalk');

class ErrorHandler extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}

const handleError = (err, res) => {
  if (!(err instanceof ErrorHandler))
    // If there is any uncaught error
    err = new ErrorHandler(500, err.message || 'Internal server error.');

  console.log(`${chalk.red('ERROR:')} ${err.message}`);

  res.status(err.statusCode).json(err);
};

module.exports = {
  ErrorHandler,
  handleError,
};
