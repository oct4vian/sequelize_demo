const { Classroom, Student } = require('../models');

const withStudents = { include: [{ model: Student, as: 'students' }] };

module.exports = {
  async getAll() {
    return Classroom.findAndCountAll({ withStudents });
  },

  async getOne(id) {
    return Classroom.findByPk(id, withStudents);
  },

  async create(class_name) {
    return Classroom.create({ class_name });
  },

  async createWithStudents(class_name, students) {
    return Classroom.create({ class_name, students }, withStudents);
  },

  async update(id, class_name) {
    const classroom = await Classroom.findByPk(id);

    if (!classroom)
      throw new Error({ message: 'No entity with the specified id' });

    return classroom.update({ class_name });
  },

  async delete(id) {
    const classroom = await Classroom.findByPk(id);

    if (!classroom)
      throw new Error({ message: 'No entity with the specified id' });

    return classroom.destroy();
  }
};
