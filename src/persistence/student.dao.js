const { Student, Classroom, Course } = require('../models');

const PAGE_SIZE = 5;
const withClassroomAndCourse = {
  include: [
    { model: Classroom, as: 'classroom' },
    { model: Course, as: 'courses', through: { attributes: [] } }
  ],
  order: [['id', 'ASC']]
};

module.exports = {
  async getAll(page) {
    return Student.findAndCountAll({
      ...withClassroomAndCourse,
      offset: page * PAGE_SIZE,
      limit: PAGE_SIZE
    });
  },

  async getOne(id) {
    return Student.findByPk(id, withClassroomAndCourse);
  },

  async create(student_name) {
    return Student.create({ student_name });
  },

  async createWithStudents(class_name, students) {
    return Classroom.create({ class_name, students }, withStudents);
  },

  async update(id, student_name) {
    const student = await Student.findByPk(id);

    if (!student)
      throw new Error({ message: 'No entity with the specified id' });

    return student.update({ student_name });
  },

  async delete(id) {
    const student = await Student.findByPk(id);

    if (!student)
      throw new Error({ message: 'No entity with the specified id' });

    return student.destroy();
  }
};
