const { Lecturer, Course } = require('../models');

const withCourse = { include: [{ model: Course, as: 'course' }] };

module.exports = {
  async getAll() {
    return Lecturer.findAndCountAll({ withCourse });
  },

  async getOne(id) {
    return Lecturer.findByPk(id, withCourse);
  },

  async create(lecturer_name) {
    return Lecturer.create({ lecturer_name });
  },

  async update(id, lecturer_name) {
    const lecturer = await Lecturer.findByPk(id);

    if (!lecturer)
      throw new Error({ message: 'No entity with the specified id' });

    return lecturer.update({ lecturer_name });
  },

  async delete(id) {
    const lecturer = await Lecturer.findByPk(id);

    if (!lecturer)
      throw new Error({ message: 'No entity with the specified id' });

    return lecturer.destroy();
  }
};
