const { Course, Student, Lecturer } = require('../models');

const withStudentAndLecturer = {
  include: [
    { model: Student, as: 'students', through: { attributes: [] } },
    { model: Lecturer, as: 'lecturer' }
  ]
};

module.exports = {
  async getAll() {
    return Course.findAndCountAll({ withStudentAndLecturer });
  },

  async getOne(id) {
    return Course.findByPk(id, withStudentAndLecturer);
  },

  async create(course_name) {
    return Course.create({ course_name });
  },

  async update(id, course_name) {
    const course = await Course.findByPk(id);

    if (!course)
      throw new Error({ message: 'No entity with the specified id' });

    return course.update({ course_name });
  },

  async delete(id) {
    const course = await Course.findByPk(id);

    if (!course)
      throw new Error({ message: 'No entity with the specified id' });

    return course.destroy();
  }
};
