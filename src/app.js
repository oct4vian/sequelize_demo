const express = require('express');
const path = require('path');

const { handleError } = require('./helpers/error');

const morganMiddleware = require('./middlewares/morganMiddleware');

const indexRouter = require('./routes/index');
const apiRouter = require('./routes/api');

const app = express();

app.use(morganMiddleware);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api', apiRouter);

// error handler
app.use((err, req, res, next) => handleError(err, res));

module.exports = app;
