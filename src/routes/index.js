const express = require('express');
const router = express.Router();

/* Render the home page. */
router.get('/', (_, res) =>
  res.render('index', { title: 'Sequelize demo project' })
);

module.exports = router;
