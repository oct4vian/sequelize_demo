const express = require('express');
const router = express.Router();

const classroomRouter = require('./classroom.router');
const studentRouter = require('./student.router');
const courseRouter = require('./course.router');
const lecturerRouter = require('./lecturer.router');

router.get('/', (_, res) => res.send('api works.'));

router.use('/classrooms', classroomRouter);
router.use('/students', studentRouter);
router.use('/lecturers', lecturerRouter);
router.use('/courses', courseRouter);

module.exports = router;
