const express = require('express');
const router = express.Router();

const { validate, studentRules } = require('../../middlewares/validators');
const { studentController: controller } = require('../../controllers');

router.get('/', controller.getAll);
router.get('/:id', controller.getOne);
router.post('/', studentRules, validate, controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);
router.post('/add_course', controller.addCourse);

module.exports = router;
