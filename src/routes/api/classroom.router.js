const express = require('express');
const router = express.Router();

const withAuthorization = require('../../middlewares/withAuthorization');
const { classroomController: controller } = require('../../controllers');

router.get('/', controller.getAll);
router.get('/:id', controller.getOne);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);

router.post(
  '/add_with_students',
  withAuthorization,
  controller.createWithStudents
);

module.exports = router;
