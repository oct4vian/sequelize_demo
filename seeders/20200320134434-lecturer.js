'use strict';

const faker = require('faker');

const lecturers = [
  {
    lecturer_name: faker.name.findName(),
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    lecturer_name: faker.name.findName(),
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    lecturer_name: faker.name.findName(),
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Lecturers', lecturers);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Lecturers', null, {});
  }
};
