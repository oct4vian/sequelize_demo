'use strict';

const classrooms = [
  { class_name: 'A1', createdAt: new Date(), updatedAt: new Date() },
  { class_name: 'B2', createdAt: new Date(), updatedAt: new Date() },
  { class_name: 'C1', createdAt: new Date(), updatedAt: new Date() }
];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Classrooms', classrooms);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Classrooms', null, {});
  }
};
