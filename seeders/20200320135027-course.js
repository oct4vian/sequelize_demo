'use strict';

const faker = require('faker');

const courses = [
  {
    lecturer_id: 2,
    course_name: faker.commerce.department(),
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    lecturer_id: 3,
    course_name: faker.commerce.department(),
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    lecturer_id: 1,
    course_name: faker.commerce.department(),
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Courses', courses);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Courses', null, {});
  }
};
