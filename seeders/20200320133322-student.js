'use strict';

const faker = require('faker');
const students = [];

for (let i = 0; i < 100; i++) {
  students.push({
    classroom_id: (i % 3) + 1,
    student_name: faker.name.findName(),
    createdAt: new Date(),
    updatedAt: new Date()
  });
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Students', students);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Students', null, {});
  }
};
